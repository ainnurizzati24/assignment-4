package assignment4;

import java.util.ArrayList;
import java.util.Iterator;

public class IterateArray {

	public static void main(String[] args) {
		ArrayList<String> names = new ArrayList<>();
		
		names.add("Ain");
		names.add("Bob");
		names.add("Po");
		
		System.out.println("Size: " + names.size());
		
		//iterator
		Iterator<String> itr = names.iterator();
		
		while(itr.hasNext()){  
			System.out.println(itr.next());  
		}
		
		System.out.println(names);
		
		//for loop
		for(int i=0;i<names.size();i++)  
        {  
         System.out.println(names.get(i));     
        } 

	}

}
