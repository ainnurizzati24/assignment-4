package assignment4;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class OpenEmail {

	public static void main(String[] args) {
		 
			WebDriver driver=new ChromeDriver();
			 
			driver.get("https://www.google.com/intl/en-GB/gmail/about/");
			driver.manage().window().maximize();
			 
			List<WebElement> allLinks = driver.findElements(By.tagName("a"));
			 
			for(WebElement link:allLinks){
			 System.out.println(link.getText() + " - " + link.getAttribute("href"));
			}
		} 
}
