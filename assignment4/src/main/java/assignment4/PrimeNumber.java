package assignment4;

import java.util.Scanner;

public class PrimeNumber {

	public static void main(String[] args) {
		
		boolean isPrime = true;
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter temp: ");
		int temp = sc.nextInt();
		
		System.out.println("Enter num: ");
		int num = sc.nextInt();
		
		for(int i=2; i<num/2; i++) {
			if(temp % 2 == 0) {
				isPrime = false;
			} else {
				isPrime = true;
			}
		}
		
		System.out.println("Is prime? : " + isPrime);
	}

}
