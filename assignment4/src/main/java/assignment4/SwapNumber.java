package assignment4;

import java.util.Scanner;

public class SwapNumber {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter x: ");
		int x = sc.nextInt();

		System.out.println("Enter y: ");
		int y = sc.nextInt();

		int temp = x;
		x = y;
		y = temp;

		System.out.println("Output");
		System.out.println("x: " + x);
		System.out.println("y: " + y);
		System.out.println("temp: " + temp);
	}

}
