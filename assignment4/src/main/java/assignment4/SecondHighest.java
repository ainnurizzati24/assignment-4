package assignment4;

import java.util.Arrays;
import java.util.Random;

public class SecondHighest {

	public static void main(String[] args) {
		Random rndm = new Random();
		
		int [] randomNumber = new int[10];
		int first = 0;
		int second = 0;
		
		for (int i = 0; i < randomNumber.length; i++) {
	         randomNumber[i] = rndm.nextInt(); 
	     }
		Arrays.sort(randomNumber);
		second = first = randomNumber[0];
		
		for (int i = 0; i < randomNumber.length; i++) { 
	         System.out.println(randomNumber[i]);
	         
	         if (randomNumber[i] > first) {
	        	 second = first;
	        	 first = randomNumber[i];
	         } else if(randomNumber[i] > second) {
	        	 second = randomNumber[i];
	         } 
	     }
		
		System.out.println("Second: " + second);
		System.out.println("First:  " + first);
	}

}
